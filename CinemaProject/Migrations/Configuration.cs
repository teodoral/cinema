namespace CinemaProject.Migrations
{
    using CinemaProject.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CinemaProject.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CinemaProject.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Formats.AddOrUpdate(x => x.Id,
                new Format { Id = 1, Name = "2D" },
                new Format { Id = 2, Name = "3D" },
                new Format { Id = 3, Name = "4D" });

            context.Theatres.AddOrUpdate(x => x.Id,
                new Theatre { Id = 1, Name = "Theatre 1", SeatsNumber = 70 },
                new Theatre { Id = 2, Name = "Theatre 2", SeatsNumber = 100 },
                new Theatre { Id = 3, Name = "Theatre 3", SeatsNumber = 100 },
                new Theatre { Id = 4, Name = "Theatre 4", SeatsNumber = 60 },
                new Theatre { Id = 5, Name = "Theatre 5", SeatsNumber = 200 });

            context.TheatreSupportedFormats.AddOrUpdate(x => x.Id,
                new TheatreSupportedFormat { Id = 1, TheatreId = 1, FormatId = 1 },
                new TheatreSupportedFormat { Id = 2, TheatreId = 2, FormatId = 1 },
                new TheatreSupportedFormat { Id = 3, TheatreId = 3, FormatId = 1 },
                new TheatreSupportedFormat { Id = 4, TheatreId = 4, FormatId = 1 },
                new TheatreSupportedFormat { Id = 5, TheatreId = 5, FormatId = 1 },
                new TheatreSupportedFormat { Id = 6, TheatreId = 1, FormatId = 2 },
                new TheatreSupportedFormat { Id = 7, TheatreId = 2, FormatId = 2 },
                new TheatreSupportedFormat { Id = 8, TheatreId = 5, FormatId = 2 },
                new TheatreSupportedFormat { Id = 9, TheatreId = 5, FormatId = 3 });


            //foreach (var theatre in context.Theatres)
            //{
            //    for (int i = 1; i <= theatre.SeatsNumber; i++)
            //    {
            //        context.Seats.AddOrUpdate(x => x.Id,
            //            new Seat { Number = i, TheatreId = theatre.Id });
            //    }
            //}

            context.Films.AddOrUpdate(x => x.Id,
                new Film { Id = 1, Title = "Pulp fiction", Director = "Quentin Tarantino", Cast = "John Travolta, Uma Thurman, Samuel L. Jackson", Genre = "Crime, Drama", Duration = 174, Distributor = "Distributor 1", Country = "USA", Year = 1994, Description = "The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption." },
                new Film { Id = 2, Title = "Shaun of the Dead", Director = "Edgar Wright", Cast = "Simon Pegg, Nick Frost, Kate Ashfield", Genre = "Comedy, Horror", Duration = 99, Distributor = "Distributor 2", Country = "UK", Year = 2004, Description = "A man's uneventful life is disrupted by the zombie apocalypse." },
                new Film { Id = 3, Title = "Death at a Funeral", Director = "Frank Oz", Cast = "Matthew Macfadyen, Peter Dinklage, Ewen Bremner", Genre = "Comedy", Duration = 90, Distributor = "Distributor 2", Country = "UK", Year = 2007, Description = "Chaos ensues when a man tries to expose a dark secret regarding a recently deceased patriarch of a dysfunctional British family." },
                new Film { Id = 4, Title = "Dunkirk", Director = "Christopher Nolan", Cast = "Fionn Whitehead, Barry Keoghan, Mark Rylance", Genre = "Action, Drama, History", Duration = 106, Distributor = "Distributor 2", Country = "UK", Year = 2017, Description = "Allied soldiers from Belgium, the British Empire, and France are surrounded by the German Army and evacuated during a fierce battle in World War II." },
                new Film { Id = 5, Title = "Blue Jasmin", Director = "Woody Allen", Cast = "Cate Blanchett, Alec Baldwin, Peter Sarsgaard", Genre = "Drama", Duration = 98, Distributor = "Distributor 1", Country = "USA", Year = 2013, Description = "A New York socialite, deeply troubled and in denial, arrives in San Francisco to impose upon her sister. She looks a million, but isn't bringing money, peace, or love..." },
                new Film { Id = 6, Title = "Gravity", Director = "Alfonso Cuar�n", Cast = "Sandra Bullock, George Clooney, Ed Harris", Genre = "Drama, Sci-Fi, Thriller", Duration = 91, Distributor = "Distributor 1", Country = "USA", Year = 2013, Description = "Two astronauts work together to survive after an accident leaves them stranded in space." });

            context.Showtimes.AddOrUpdate(x => x.Id,
                new Showtime { Id = 1, Time = new DateTime(2020, 12, 1, 19, 00, 00), Price = 300, FilmId = 1, FormatId = 1, TheatreId = 1 },
                new Showtime { Id = 2, Time = new DateTime(2020, 12, 3, 19, 00, 00), Price = 300, FilmId = 1, FormatId = 1, TheatreId = 1 },
                new Showtime { Id = 3, Time = new DateTime(2020, 12, 5, 19, 00, 00), Price = 300, FilmId = 1, FormatId = 1, TheatreId = 1 },
                new Showtime { Id = 4, Time = new DateTime(2020, 12, 1, 20, 30, 00), Price = 400, FilmId = 6, FormatId = 2, TheatreId = 2 },
                new Showtime { Id = 5, Time = new DateTime(2020, 12, 3, 20, 30, 00), Price = 400, FilmId = 6, FormatId = 2, TheatreId = 2 },
                new Showtime { Id = 6, Time = new DateTime(2020, 12, 5, 20, 30, 00), Price = 400, FilmId = 6, FormatId = 2, TheatreId = 2 },
                new Showtime { Id = 7, Time = new DateTime(2020, 12, 1, 19, 30, 00), Price = 250, FilmId = 2, FormatId = 1, TheatreId = 4 },
                new Showtime { Id = 8, Time = new DateTime(2020, 12, 3, 19, 30, 00), Price = 250, FilmId = 2, FormatId = 1, TheatreId = 4 },
                new Showtime { Id = 9, Time = new DateTime(2020, 12, 5, 19, 30, 00), Price = 250, FilmId = 2, FormatId = 1, TheatreId = 4 },
                new Showtime { Id = 10, Time = new DateTime(2020, 12, 2, 20, 00, 00), Price = 350, FilmId = 4, FormatId = 2, TheatreId = 1 },
                new Showtime { Id = 11, Time = new DateTime(2020, 12, 4, 20, 00, 00), Price = 350, FilmId = 4, FormatId = 2, TheatreId = 1 },
                new Showtime { Id = 12, Time = new DateTime(2020, 12, 6, 20, 00, 00), Price = 350, FilmId = 4, FormatId = 2, TheatreId = 1 });

        }
    }
}
