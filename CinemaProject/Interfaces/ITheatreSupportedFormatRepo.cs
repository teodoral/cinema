﻿using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Interfaces
{
    public interface ITheatreSupportedFormatRepo
    {
        IEnumerable<TheatreSupportedFormat> GetAll();
        TheatreSupportedFormat GetById(int id);
        void Add(TheatreSupportedFormat supportedFormat);
        void Remove(TheatreSupportedFormat supportedFormat);
        bool CheckExistence(int theatreId, int formatId);
    }
}
