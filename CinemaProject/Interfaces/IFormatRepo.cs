﻿using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject
{
    public interface IFormatRepo
    {
        IEnumerable<Format> GetFormats();
        Format GetFormatById(int id);
        IEnumerable<Format> GetSupportedFormats(int theatreId);
    }
}
