﻿using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Interfaces
{
    public interface ISeatRepo
    {
        IEnumerable<Seat> GetByTheatre(int theatreId);
        Seat GetSeatById(int id);
    }
}
