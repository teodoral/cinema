﻿using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Interfaces
{
    public interface IShowtimeRepo
    {
        IEnumerable<Showtime> GetShowtimes(string sortTypes);
        IEnumerable<Showtime> GetShowtimesToday(DateTime date);
        Showtime GetShowtimeById(int id);
        IEnumerable<Showtime> ShowtimesFilteredSearch(ShowtimeFilter filter);
        void Add(Showtime showtime);
        void Delete(Showtime showtime);
        //int NumberOfFreePlaces(Showtime shoetime);
        //IEnumerable<Showtime> GetFreeShowtimes();
    }
}
