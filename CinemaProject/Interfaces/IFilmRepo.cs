﻿using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Interfaces
{
    public interface IFilmRepo
    {
        IEnumerable<Film> GetFilms(string sortType);
        Film GetFilmById(int id);
        IEnumerable<Film> FilmsFilteredSearch(FilmFilter filter);
        void Add(Film film);
        void Edit(Film film);
        void Delete(Film film);
    }
}
