﻿using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Interfaces
{
    public interface ITicketsRepo
    {
        IEnumerable<Ticket> GetTickets();
        IEnumerable<Ticket> GetTicketsById(int id);
        IEnumerable<Ticket> GetTicketsByUser(ApplicationUser user);
        IEnumerable<Ticket> GetTicketsByShowtime(Showtime showtime);
        void Add(Ticket ticket);
        void Delete(Ticket ticket);
    }
}
