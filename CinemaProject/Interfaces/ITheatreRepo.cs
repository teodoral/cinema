﻿using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Interfaces
{
    public interface ITheatreRepo
    {
        IEnumerable<Theatre> GetTheatres();
        IEnumerable<TheatreDTO> GetTheatresDetailed();
        Theatre GetTheatreById(int id);
        TheatreDTO GetTheatreDetailed(int id);
        IEnumerable<Theatre> FilterBySupportedFormat(int formatId);
    }
}
