﻿using CinemaProject.Interfaces;
using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CinemaProject.Controllers
{
    public class TheatresController : ApiController
    {
        ITheatreRepo _repository { get; set; }

        public TheatresController(ITheatreRepo repository)
        {
            _repository = repository;
        }

        public IEnumerable<Theatre> Get()
        {
            return _repository.GetTheatres();
        }

        [Route("api/theatresdetails")]
        public IEnumerable<TheatreDTO> GetDetails()
        {
            return _repository.GetTheatresDetailed();
        }

        public IHttpActionResult Get(int id)
        {
            var theatre = _repository.GetTheatreById(id);
            if(theatre == null)
            {
                return NotFound();
            }
            return Ok(theatre);
        }

        [Route("api/theatresdetails/{id}")]
        public IHttpActionResult GetTheatreDetalsById(int id)
        {
            var theatreDetails = _repository.GetTheatreDetailed(id);
            if(theatreDetails == null)
            {
                return NotFound();
            }
            return Ok(theatreDetails);
        }

        [Route("api/filteredbyformat")]
        public IEnumerable<Theatre> Post(Format format)
        {
            int id = format.Id;
            return _repository.FilterBySupportedFormat(id);
        }
    }
}
