﻿using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CinemaProject.Controllers
{
    public class FormatsController : ApiController
    {
        public IFormatRepo _repository { get; set; }

        public FormatsController(IFormatRepo repository)
        {
            _repository = repository;
        }

        public IEnumerable<Format> Get()
        {
            return _repository.GetFormats();
        }

        public IHttpActionResult Get(int id)
        {
            var format = _repository.GetFormatById(id);
            if(format == null)
            {
                return NotFound();
            }
            return Ok(format);
        }

        [Route("api/supportedformats")]
        public IEnumerable<Format> Post(Theatre theatre)
        {
            int id = theatre.Id;
            return _repository.GetSupportedFormats(id);
        }
    }
}
