﻿using CinemaProject.Interfaces;
using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CinemaProject.Controllers
{
    public class TheatreSupportedFormatsController : ApiController
    {
        ITheatreSupportedFormatRepo _repository { get; set; }

        public TheatreSupportedFormatsController(ITheatreSupportedFormatRepo repository)
        {
            _repository = repository;
        }

        public IEnumerable<TheatreSupportedFormat> Get()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var supportedFormat = _repository.GetById(id);
            if(supportedFormat == null)
            {
                return NotFound();
            }
            return Ok(supportedFormat);
        }

        private bool Check(int theatreId, int formatId)
        {
            return _repository.CheckExistence(theatreId, formatId);
        }

        public IHttpActionResult Post(TheatreSupportedFormat supportedFormat)
        {
            string errorMessage = "Format is already supported in theatre";
            
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            if (Check(supportedFormat.TheatreId, supportedFormat.FormatId))
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, errorMessage));
            }
            _repository.Add(supportedFormat);
            return CreatedAtRoute("DefaultApi", new { id = supportedFormat.Id }, supportedFormat);
        }

        public IHttpActionResult Delete(int id)
        {
            var supportedFormat = _repository.GetById(id);
            if(supportedFormat == null)
            {
                return NotFound();
            }
            _repository.Remove(supportedFormat);
            return Ok();
        }
    }
}
