﻿using CinemaProject.Interfaces;
using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CinemaProject.Controllers
{
    public class ShowtimesController : ApiController
    {
        IShowtimeRepo _repository { get; set; }

        public ShowtimesController(IShowtimeRepo repository)
        {
            _repository = repository;
        }

        public IEnumerable<Showtime> Get(string sortType = "Date")
        {
            return _repository.GetShowtimes(sortType);
        }

        public IHttpActionResult Get(int id)
        {
            var showtime = _repository.GetShowtimeById(id);
            if(showtime == null)
            {
                return NotFound();
            }
            return Ok(showtime);
        }

        public IHttpActionResult Post(Showtime showtime)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.Add(showtime);
            return CreatedAtRoute("DefaultApi", new { id = showtime.Id }, showtime);
        }

        [Route("api/today")]
        public IEnumerable<Showtime> GetToday()
        {
            return _repository.GetShowtimesToday(DateTime.Today);
        }

        public IHttpActionResult Delete(int id)
        {
            var showtime = _repository.GetShowtimeById(id);
            if(showtime == null)
            {
                return NotFound();
            }
            _repository.Delete(showtime);
            return Ok();
        }

        [Route("api/filteredshowtimes")]
        public IEnumerable<Showtime> Post(ShowtimeFilter filter)
        {
            return _repository.ShowtimesFilteredSearch(filter);
        }
    }
}
