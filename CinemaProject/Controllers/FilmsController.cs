﻿using CinemaProject.Interfaces;
using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CinemaProject.Controllers
{
    public class FilmsController : ApiController
    {
        IFilmRepo _repository { get; set; }

        public FilmsController(IFilmRepo repository)
        {
            _repository = repository;
        }

        public IEnumerable<Film> Get(string sortType = "Title")
        {
            return _repository.GetFilms(sortType);
        }

        public IHttpActionResult Get(int id)
        {
            var film = _repository.GetFilmById(id);
            if (film == null)
            {
                return NotFound();
            }
            return Ok(film);
        }

        private bool FilmExists(int id)
        {
            return _repository.GetFilmById(id) != null;
        }

        public IHttpActionResult Put(int id, Film film)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != film.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Edit(film);
            }
            catch
            {
                if (!FilmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(film);
        }

        [Route("api/filteredfilms")]
        public IEnumerable<Film> Post(FilmFilter filter)
        {
            return _repository.FilmsFilteredSearch(filter);
        }
    }
}
