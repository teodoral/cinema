﻿using CinemaProject.Interfaces;
using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaProject.Repository
{
    public class TheatreRepo : IDisposable, ITheatreRepo
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public IEnumerable<Theatre> FilterBySupportedFormat (int formatId)
        {
            var filteredTheatreIds = db.TheatreSupportedFormats.Where(x => x.FormatId == formatId).Select(x => x.TheatreId);
            return db.Theatres.Where(x => filteredTheatreIds.Contains(x.Id));
        }

        public Theatre GetTheatreById(int id)
        {
            return db.Theatres.Find(id);
        }

        public TheatreDTO GetTheatreDetailed(int id)
        {
            var theatreDetail = db.TheatreSupportedFormats.Where(x => x.TheatreId == id).GroupBy(
                x => x.Theatre,
                x => x.Format,
                (theatre, formats) => new TheatreDTO
                {
                    Theatre = theatre,
                    SupportedFormats = formats
                }).SingleOrDefault();
            return theatreDetail;
        }

        public IEnumerable<Theatre> GetTheatres()
        {
            return db.Theatres;
        }

        public IEnumerable<TheatreDTO> GetTheatresDetailed()
        {
            var theatresDetail = db.TheatreSupportedFormats.GroupBy(
                x => x.Theatre,
                x => x.Format,
                (theatre, formats) => new TheatreDTO
                {
                    Theatre = theatre,
                    SupportedFormats = formats
                });
            return theatresDetail;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}