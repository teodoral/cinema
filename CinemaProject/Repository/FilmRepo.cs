﻿using CinemaProject.Interfaces;
using CinemaProject.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace CinemaProject.Repository
{
    public class FilmRepo : IDisposable, IFilmRepo
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Film film)
        {
            db.Films.Add(film);
            db.SaveChanges();
        }

        public void Delete(Film film)
        {
            db.Films.Remove(film);
            db.SaveChanges();
        }

        public void Edit(Film film)
        {
            db.Entry(film).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public Film GetFilmById(int id)
        {
            return db.Films.Find(id);
        }

        // enumeracija za razlicite kriterijum sortiranja
        private enum SortTypes
        {
            Title,
            TitleDesc,
            Genre,
            GenreDesc,
            Duration,
            DurationDesc,
            Year,
            YearDesc,
            Distributor,
            DistributorDesc,
            Country,
            CountryDesc
        };

        // parovi kljuc(string) - vrednost(sortType) za odabir kriterijuma za sortiranje na osnovu prosledjenog stringa - vidi sledecu metodu
        private static Dictionary<string, SortTypes> SortTypeDict = new Dictionary<string, SortTypes>
        {
            {"Title" , SortTypes.Title },
            {"Title Descending" , SortTypes.TitleDesc },
            {"Genre" , SortTypes.Genre },
            {"Genre Descending" , SortTypes.GenreDesc },
            {"Duration" , SortTypes.Duration },
            {"Duration Descending" , SortTypes.DurationDesc },
            {"Year" , SortTypes.Year },
            {"Year Descending" , SortTypes.YearDesc },
            {"Distributor" , SortTypes.Distributor },
            {"Distributor Descending" , SortTypes.DistributorDesc },
            {"Country" , SortTypes.Country },
            {"Country Descending" , SortTypes.CountryDesc }

        };

        /* vraca sve filmove sortirane prema odabranom kriterijumu (argument metode)
         * sortType je po defaultu naziv filma (dodeljen u kontroleru */
        public IEnumerable<Film> GetFilms(string sortType)
        {
            SortTypes sortBy = SortTypeDict[sortType];

            IQueryable<Film> films = db.Films;

            switch (sortBy)
            {
                case SortTypes.Title:
                    films = films.OrderBy(x => x.Title);
                    break;
                case SortTypes.TitleDesc:
                    films = films.OrderByDescending(x => x.Title);
                    break;
                case SortTypes.Genre:
                    films = films.OrderBy(x => x.Genre);
                    break;
                case SortTypes.GenreDesc:
                    films = films.OrderByDescending(x => x.Genre);
                    break;
                case SortTypes.Duration:
                    films = films.OrderBy(x => x.Duration);
                    break;
                case SortTypes.DurationDesc:
                    films = films.OrderByDescending(x => x.Duration);
                    break;
                case SortTypes.Year:
                    films = films.OrderBy(x => x.Year);
                    break;
                case SortTypes.YearDesc:
                    films = films.OrderByDescending(x => x.Year);
                    break;
                case SortTypes.Distributor:
                    films = films.OrderBy(x => x.Distributor);
                    break;
                case SortTypes.DistributorDesc:
                    films = films.OrderByDescending(x => x.Distributor);
                    break;
                case SortTypes.Country:
                    films = films.OrderBy(x => x.Country);
                    break;
                case SortTypes.CountryDesc:
                    films = films.OrderByDescending(x => x.Country);
                    break;
            }

            return films;
        }

        // vraca filmove filtrirane prema razlicitim kriterijumima
        public IEnumerable<Film> FilmsFilteredSearch(FilmFilter filter)
        {
            var result = GetFilms("Title");
            if (!filter.TitleFilter.IsNullOrWhiteSpace())
            {
                result = result.Where(x => x.Title.ToLower().Contains(filter.TitleFilter.ToLower()));
            }
            if (!filter.GenreFilter.IsNullOrWhiteSpace())
            {
                result = result.Where(x => x.Genre.ToLower().Contains(filter.GenreFilter.ToLower()));
            }
            if (filter.MinDurationFilter > 0)
            {
                result = result.Where(x => x.Duration >= filter.MinDurationFilter);
            }
            if (filter.MaxDurationFilter > 0)
            {
                result = result.Where(x => x.Duration <= filter.MaxDurationFilter);
            }
            if (filter.MinYearFilter > 0)
            {
                result = result.Where(x => x.Year >= filter.MinYearFilter);
            }
            if (filter.MaxYearFilter > 0)
            {
                result = result.Where(x => x.Year <= filter.MaxYearFilter);
            }
            if (!filter.DistributorFilter.IsNullOrWhiteSpace())
            {
                result = result.Where(x => x.Distributor.ToLower().Contains(filter.DistributorFilter.ToLower()));
            }
            if (!filter.CountryFilter.IsNullOrWhiteSpace())
            {
                result = result.Where(x => x.Country.ToLower().Contains(filter.CountryFilter.ToLower()));
            }
            return result;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}