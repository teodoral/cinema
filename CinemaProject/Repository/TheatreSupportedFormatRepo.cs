﻿using CinemaProject.Interfaces;
using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CinemaProject.Repository
{
    public class TheatreSupportedFormatRepo : IDisposable, ITheatreSupportedFormatRepo
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public bool CheckExistence(int theatreId, int formatId)
        {
            return db.TheatreSupportedFormats.Where(x => (x.TheatreId == theatreId) && (x.FormatId == formatId)).SingleOrDefault() != null;
        }

        public void Add(TheatreSupportedFormat supportedFormat)
        {
            db.TheatreSupportedFormats.Add(supportedFormat);
            db.SaveChanges();
        }

        public IEnumerable<TheatreSupportedFormat> GetAll()
        {
            return db.TheatreSupportedFormats.Include(x => x.Theatre).Include(x => x.Format);
        }

        public TheatreSupportedFormat GetById(int id)
        {
            return db.TheatreSupportedFormats.Where(x => x.Id == id).SingleOrDefault();
        }

        public void Remove(TheatreSupportedFormat supportedFormat)
        {
            db.TheatreSupportedFormats.Remove(supportedFormat);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}