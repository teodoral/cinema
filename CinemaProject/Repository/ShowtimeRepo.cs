﻿using CinemaProject.Interfaces;
using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Microsoft.Ajax.Utilities;

namespace CinemaProject.Repository
{
    public class ShowtimeRepo : IDisposable, IShowtimeRepo
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Showtime showtime)
        {
            db.Showtimes.Add(showtime);
            db.SaveChanges();
        }

        public void Delete(Showtime showtime)
        {
            db.Showtimes.Remove(showtime);
            db.SaveChanges();
        }

        public Showtime GetShowtimeById(int id)
        {
            return db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).Where(x => x.Id == id).SingleOrDefault();
        }

        private enum SortTypes
        {
            FilmTitle,
            FilmTitleDesc,
            Date,
            DateDesc,
            Time,
            TimeDesc,
            Format,
            FormatDesc,
            Price,
            PriceDesc
        }

        private Dictionary<string, SortTypes> SortTypesDict = new Dictionary<string, SortTypes>()
        {
            {"Film Title", SortTypes.FilmTitle },
            {"Film Title Descending", SortTypes.FilmTitleDesc },
            {"Date", SortTypes.Date },
            {"Date Descending", SortTypes.DateDesc },
            {"Time", SortTypes.Time },
            {"Time Descending", SortTypes.TimeDesc },
            {"Format", SortTypes.Format },
            {"Format Descending", SortTypes.FormatDesc },
            {"Price", SortTypes.Price },
            {"Price Descending", SortTypes.PriceDesc }
        };

        public IEnumerable<Showtime> GetShowtimes(string sortTypes)
        {
            SortTypes sortBy = SortTypesDict[sortTypes];

            IEnumerable<Showtime> showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre);

            switch (sortBy)
            {
                case SortTypes.FilmTitle:
                    showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderBy(x => x.Film.Title);
                    break;
                case SortTypes.FilmTitleDesc:
                    showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderByDescending(x => x.Film.Title);
                    break;
                case SortTypes.Date:
                    showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderBy(x => DbFunctions.CreateDateTime(x.Time.Year, x.Time.Month, x.Time.Day, x.Time.Hour, x.Time.Minute, x.Time.Second));
                    break;
                case SortTypes.DateDesc:
                    showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderByDescending(x => DbFunctions.CreateDateTime(x.Time.Year, x.Time.Month, x.Time.Day, x.Time.Hour, x.Time.Minute, x.Time.Second));
                    break;
                case SortTypes.Time:
                    showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderBy(x => DbFunctions.CreateTime(x.Time.Hour, x.Time.Minute, x.Time.Second));
                    break;
                case SortTypes.TimeDesc:
                    showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderByDescending(x => DbFunctions.CreateTime(x.Time.Hour, x.Time.Minute, x.Time.Second));
                    break;
                case SortTypes.Format:
                    showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderBy(x => x.Format.Name);
                    break;
                case SortTypes.FormatDesc:
                    showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderByDescending(x => x.Format.Name);
                    break;
                case SortTypes.Price:
                    showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderBy(x => x.Price);
                    break;
                case SortTypes.PriceDesc:
                    showtimes = db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderByDescending(x => x.Price);
                    break;
            }
            return showtimes;
        }

        public IEnumerable<Showtime> GetShowtimesToday(DateTime date)
        {
            List<Showtime> result = new List<Showtime>();
            foreach (var showtime in db.Showtimes.Include(x => x.Film).Include(x => x.Format).Include(x => x.Theatre).OrderBy(x => x.Time))
            {
                if (showtime.Time.Date == date.Date)
                {
                    result.Add(showtime);
                }
            }
            return result;
        }

        public IEnumerable<Showtime> ShowtimesFilteredSearch(ShowtimeFilter filter)
        {
            var showtimes = GetShowtimes("Date");
            if (!filter.FilmFilter.IsNullOrWhiteSpace())
            {
                showtimes = showtimes.Where(x => x.Film.Title.ToLower().Contains(filter.FilmFilter.ToLower()));
            }
            if (filter.DateStartFilter != DateTime.MinValue)
            {
                showtimes = showtimes.Where(x => x.Time.Date >= filter.DateStartFilter);
            }
            if (filter.DateEndFilter != DateTime.MinValue)
            {
                showtimes = showtimes.Where(x => x.Time.Date <= filter.DateEndFilter);
            }
            if (filter.StartTimeFilter != DateTime.MinValue.TimeOfDay)
            {
                showtimes = showtimes.Where(x => x.Time.TimeOfDay >= filter.StartTimeFilter);
            }
            if (filter.EndTimeFilter != DateTime.MinValue.TimeOfDay)
            {
                showtimes = showtimes.Where(x => x.Time.TimeOfDay <= filter.EndTimeFilter);
            }
            if (filter.MinPriceFilter > 0)
            {
                showtimes = showtimes.Where(x => x.Price >= filter.MinPriceFilter);
            }
            if (filter.MaxPriceFilter > 0)
            {
                showtimes = showtimes.Where(x => x.Price <= filter.MaxPriceFilter);
            }
            return showtimes;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}