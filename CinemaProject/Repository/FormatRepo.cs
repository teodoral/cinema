﻿using CinemaProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaProject.Repository
{
    public class FormatRepo : IDisposable, IFormatRepo
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public Format GetFormatById(int id)
        {
            return db.Formats.Find(id);
        }

        public IEnumerable<Format> GetFormats()
        {
            return db.Formats;
        }

        public IEnumerable<Format> GetSupportedFormats(int theatreId)
        {
            var filteredFormatIds = db.TheatreSupportedFormats.Where(x => x.TheatreId == theatreId).Select(x => x.FormatId);
            return db.Formats.Where(x => filteredFormatIds.Contains(x.Id));
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}