﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using CinemaProject.Interfaces;
using CinemaProject.Models;
using CinemaProject.Repository;
using CinemaProject.Resolver;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Serialization;

namespace CinemaProject
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Unity
            var container = new UnityContainer();
            container.RegisterType<IFormatRepo, FormatRepo>(new HierarchicalLifetimeManager());
            container.RegisterType<ITheatreRepo, TheatreRepo>(new HierarchicalLifetimeManager());
            container.RegisterType<ITheatreSupportedFormatRepo, TheatreSupportedFormatRepo>(new HierarchicalLifetimeManager());
            container.RegisterType<IFilmRepo, FilmRepo>(new HierarchicalLifetimeManager());
            container.RegisterType<IShowtimeRepo, ShowtimeRepo>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);
        }
    }
}
