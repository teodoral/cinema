﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaProject.Models
{
    public class TheatreDTO
    {
        public Theatre Theatre { get; set; }
        public IEnumerable<Format> SupportedFormats { get; set; }
    }
}