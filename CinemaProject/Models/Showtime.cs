﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaProject.Models
{
    public class Showtime
    {
        public int Id { get; set; }
        [Required]
        public DateTime Time { get; set; }
        [Required]
        public int Price { get; set; }
        public Film Film { get; set; }
        public Format Format { get; set; }
        public Theatre Theatre { get; set; }
        [Required]
        public int FilmId { get; set; }
        [Required]
        public int FormatId { get; set; }
        [Required]
        public int TheatreId { get; set; }
    }
}