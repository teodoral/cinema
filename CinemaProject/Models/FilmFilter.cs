﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaProject.Models
{
    public class FilmFilter
    {
        public string TitleFilter { get; set; }
        public string GenreFilter { get; set; }
        public int MinDurationFilter { get; set; }
        public int MaxDurationFilter { get; set; }
        public int MinYearFilter { get; set; }
        public int MaxYearFilter { get; set; }
        public string DistributorFilter { get; set; }
        public string CountryFilter { get; set; }
    }
}