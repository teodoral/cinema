﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaProject.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public DateTime SaleTime { get; set; }
        public Showtime Showtime { get; set; }
        public Seat Seat { get; set; }
        [Required]
        public int ShowtimeId { get; set; }
        [Required]
        public int SeatId { get; set; }
    }
}