﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaProject.Models
{//pretragu (filtriranje) projekcija po filmu, opsegu datuma i vremena, tipu projekcije (omogućiti odabir među ponuđenim vrednostima),
    // sali (omogućiti odabir među ponuđenim vrednostima),
    // i opsegu cena karata, kao i kombinacijama svih ovih kriterijuma.
    public class ShowtimeFilter
    {
        public string FilmFilter { get; set; }
        public DateTime DateStartFilter { get; set; }
        public DateTime DateEndFilter { get; set; }
        public TimeSpan StartTimeFilter { get; set; }
        public TimeSpan EndTimeFilter { get; set; }
        public string FormatFilter { get; set; }
        public int MinPriceFilter { get; set; }
        public int MaxPriceFilter { get; set; }
    }
}