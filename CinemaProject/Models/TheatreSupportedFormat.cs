﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaProject.Models
{
    public class TheatreSupportedFormat
    {
        public int Id { get; set; }
        public Format Format { get; set; }

        public Theatre Theatre { get; set; }

        public int FormatId { get; set; }

        public int TheatreId { get; set; }
    }
}