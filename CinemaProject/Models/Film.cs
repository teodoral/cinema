﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaProject.Models
{
    public class Film
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }

        public string Director { get; set; }

        public string Cast { get; set; }

        public string Genre { get; set; }
        [Required]
        [Range(1, Int32.MaxValue, ErrorMessage = "Duration must be at least 1 minute.")]
        public int Duration { get; set; }
        [Required]
        public string Distributor { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        [Range(1, Int32.MaxValue, ErrorMessage = "Year must be greater than 0.")]
        public int Year { get; set; }
        public string Description { get; set; }
    }
}