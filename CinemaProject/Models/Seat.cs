﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaProject.Models
{
    public class Seat
    {
        public int Id { get; set; }
        [Required]
        public int Number { get; set; }
        public Theatre Theatre { get; set; }
        [Required]
        public int TheatreId { get; set; }
    }
}