﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaProject.Models
{
    public class Format
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}